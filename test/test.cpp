// SPDX-License-Identifier: GPL-3.0-only
#define BOOST_TEST_MODULE variant json test
#include <boost/test/unit_test.hpp>
#include <boost/hana.hpp>
#include <vector>

#include "variant_json_helper.hpp"

using namespace variant_json_helper;

struct type1 {};
struct type2 {};
struct type3 {
  int field1;
  std::string field2;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(type3, field1, field2);
VARIANT_JSON_HELPER_EMPTY_CLASS_NON_INTRUSIVE(type1);
VARIANT_JSON_HELPER_EMPTY_CLASS_NON_INTRUSIVE(type2);

const auto str1 = BOOST_HANA_STRING("T1");
const auto str2 = BOOST_HANA_STRING("T2");
const auto str3 = BOOST_HANA_STRING("T3");

json_from_to_fun<variant_member_name<type1, decltype(str1)>,
                 variant_member_name<type3, decltype(str3)>>
    codec3;

NLOHMANN_JSON_NAMESPACE_BEGIN
template <>
struct adl_serializer<std::variant<type1, type2>, void>
    : json_from_to_fun<variant_member_name<type1, decltype(str1)>,
                       variant_member_name<type2, decltype(str2)>> {};
NLOHMANN_JSON_NAMESPACE_END

BOOST_AUTO_TEST_SUITE(json_decoder_test)

BOOST_AUTO_TEST_CASE(properly_decodes_empty_classes_first) {
  nlohmann::json j{{"type", "T1"}};

  std::variant<type1, type2> val;
  j.get_to(val);

  BOOST_REQUIRE(std::holds_alternative<type1>(val));
}

BOOST_AUTO_TEST_CASE(properly_decodes_empty_classes_second) {
  nlohmann::json j2{{"type", "T2"}};

  std::variant<type1, type2> val2;
  j2.get_to(val2);
  BOOST_REQUIRE(std::holds_alternative<type2>(val2));
}

BOOST_AUTO_TEST_CASE(throws_exception_if_unknown_type) {
  nlohmann::json j2{{"type", "T3"}};

  std::variant<type1, type2> val;
  BOOST_REQUIRE_THROW(j2.get_to(val), unknown_json_type_exception);
}

BOOST_AUTO_TEST_CASE(cannot_serialize_with_array) {
  const auto vint = BOOST_HANA_STRING("vint");
  json_from_to_fun<variant_member_name<std::vector<int>, decltype(vint)>,
                   variant_member_name<type1, decltype(str1)>>
      helper;

  std::variant<std::vector<int>, type1> var = std::vector<int>{1, 2, 3};
  nlohmann::json j;
  BOOST_REQUIRE_THROW(helper.to_json(j, var), type_serialized_to_non_object);
}

BOOST_AUTO_TEST_CASE(encodes_empty_class_t1) {
  std::variant<type1, type2> var = type1();

  nlohmann::json j;

  j = var;

  nlohmann::json expected{{"type", "T1"}};
  BOOST_REQUIRE_EQUAL(j, expected);
}

BOOST_AUTO_TEST_CASE(encodes_empty_class_t2) {
  std::variant<type1, type2> var = type2();

  nlohmann::json j;

  j = var;

  nlohmann::json expected{{"type", "T2"}};
  BOOST_REQUIRE_EQUAL(j, expected);
}

BOOST_AUTO_TEST_CASE(serialize_non_empty_type) {
  std::variant<type1, type3> var = type3{42, "xyzzy"};
  nlohmann::json j;
  codec3.to_json(j, var);
  nlohmann::json expected{{"type", "T3"}, {"field1", 42}, {"field2", "xyzzy"}};
  BOOST_REQUIRE_EQUAL(j, expected);
}

BOOST_AUTO_TEST_CASE(deserialize_non_empty_type) {
  nlohmann::json j{{"type", "T3"}, {"field1", 42}, {"field2", "xyzzy"}};

  std::variant<type1, type3> got;
  codec3.from_json(j, got);
  BOOST_REQUIRE(std::holds_alternative<type3>(got));
  BOOST_REQUIRE_EQUAL(42, std::get<type3>(got).field1);
  BOOST_REQUIRE_EQUAL("xyzzy", std::get<type3>(got).field2);
}

BOOST_AUTO_TEST_SUITE_END()
