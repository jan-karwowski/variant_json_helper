# Variant Json Helper

TODO

## The problem

Suppose that we have such data types:

```C++
struct T1 {
 std::string field1;
 int field2;
};

struct T2 {
 std::string field1;
 std::string field2;
};

typedef std::variant<T1, T2> MyType;

```
and we want to have a JSON serializer for `MyType`. If we have such json:
```json
{
  "field1": "xyz",
  "field2": 3
}
```
converting from this JSON to object `MyType` based on field names leads to ambiguity. Of course in this particular case we can guess the actual type based on type of field2 but it is kinda tricky solution. 

Imagine that we have slightly different definitions:
```C++
struct T1 {};

struct T2 {};

struct T3 {
 std::string field1;
};


typedef std::variant<T1, T2, T3> MyType;

```
In this case it is impossible to distinguish between `T1` and `T2` in JSON serialized form.


## The solution

This library solves this problem by providing a serialization code for `std::variant` to be used with [nlohmann/json](https://github.com/nlohmann/json). 
The generated json extends the object with one field `"type"`. For each member of `std::variant` different string value for "type" field is assigned.
So for the three-variant code above we can wave something like that:
```json
{
  "type": "t1"
}
{
 "type": "t2"
}
{ 
 "type": "t3",
 "field1": "xyzzy"
}
```

## Usage
The example:
```C++
struct T1 {};

struct T2 {};

struct T3 {
 std::string field1;
};

// Left out: definitions of from/to_json for each of T1, T2, T3

typedef std::variant<T1, T2, T3> MyType;

// The main magic comes after this line:
const auto str1 = BOOST_HANA_STRING("T1");
const auto str2 = BOOST_HANA_STRING("T2");
const auto str3 = BOOST_HANA_STRING("T3");

NLOHMANN_JSON_NAMESPACE_BEGIN
template <>
struct adl_serializer<std::variant<T1, T2, T3>, void>
    : json_from_to_fun<variant_member_name<T1, decltype(str1)>,
                       variant_member_name<T2, decltype(str2)>,
                       variant_member_name<T3, decltype(str3)>> {};
NLOHMANN_JSON_NAMESPACE_END

```

You have to implement `adl_serializer` specialization (see nlohmann json documentation for explanation of `adl_serializer`) by public inheritance from `json_from_to_fun` parameterized with `variant_member_name` types. Each `variant_member_name` must correspond to the respective type in `std::variant<>` you want to serialize. The first template parameter is the member type of the variant. The second one is a string type (any type that has static `const char* StrType::c_str()` method that defines the value for `type` field in the generated json. The `variant_member_name`s must be in the same order as types in `std_variant`.

## Limitations

- Member types of variant must serialize to objects. Arrays, numbers, strings are not allowed. This is not checked during the compilation but will result in runtime exception `type_serialized_to_non_object` during the serialization.
- The serialized types **must not** contain `type` field. If they do it will be overwritten without any notice.
