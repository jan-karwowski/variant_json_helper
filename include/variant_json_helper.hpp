// SPDX-License-Identifier: GPL-3.0-only
#ifndef VARIANT_JSON_HELPER_H
#define VARIANT_JSON_HELPER_H

#include <concepts>
#include <exception>
#include <sstream>
#include <string>
#include <tuple>
#include <type_traits>
#include <variant>

#include <nlohmann/json.hpp>

/** \brief Generates from_json and to_json functions for a \a Type that does not
 * contain any fields. */
#define VARIANT_JSON_HELPER_EMPTY_CLASS_NON_INTRUSIVE(Type)                    \
  void from_json([[maybe_unused]] const nlohmann::json &j,                     \
                 [[maybe_unused]] Type &t) {}                                  \
  void to_json(nlohmann::json &j, [[maybe_unused]] const Type &t) {            \
    j = nlohmann::json::object();                                              \
  }

namespace variant_json_helper {

/** \brief Requirements for a type containing string for \ref
 * variant_member_type
 *
 * Boost Hana strings satisfy this requirements.
 */
template <class T>
concept string_type = requires() {
  { T::c_str() } -> std::convertible_to<const char *>;
};

/** \brief exception thrown when unknown type was found during from_json
   conversion

    The json has to contain a field "type": "somestr". If "somestr" in not one
   of the expected strings, this exception is thrown.  */
class unknown_json_type_exception : public std::exception {
  std::string what_message;

public:
  inline unknown_json_type_exception(const std::string &type) {
    std::stringstream ss;
    ss << "Unknown json type value:" << type;
    what_message = ss.str();
  }

  virtual const char *what() const noexcept { return what_message.c_str(); }
};

/** \brief thrown in the object being part of std::variant serializes to
 * something that is not a JSON object
 */
struct type_serialized_to_non_object : public std::exception {
  nlohmann::json json;
  std::string msg;

  type_serialized_to_non_object(const nlohmann::json &json) : json(json) {
    std::stringstream ss;
    ss << "The type serialized to json is not an object";
    ss << json;
    msg = ss.str();
  }

  virtual const char *what() const noexcept { return msg.c_str(); }
};

/** \brief A type tag binding a member type of the serialized/deserialized
 * std::variant with an string which is a name representing it in JSON

 @tparam T the memeber type of variant
 @tparam name \ref string_type with the name of type used in JSON */
template <class T, string_type name> struct variant_member_name {
  /** Extract a type of the variant member */
  using member_type = T;
  /** Extract a name string */
  static constexpr const char *c_str() noexcept { return name::c_str(); }
};

static const std::string TYPE_FIELD_NAME = "type";

/** \brief The class implementing a nlohmann from_json/to_json for std::variant

    @tparam Ts types of \ref variant_member_name describing name for each
   variant member.
 */
template <class... Ts> struct json_from_to_fun {
  template <class T> static constexpr const char *get_name_for_t() {
    return get_name_for_t_impl<T, Ts...>();
  }

  static void
  to_json(nlohmann::json &j,
          const std::variant<typename Ts::member_type...> &instance) {
    std::visit(
        [&j](auto &&arg) {
          using T = std::decay_t<decltype(arg)>;
          j = arg;
          if (!j.is_object()) {
            throw type_serialized_to_non_object(j);
          }
          j.push_back({TYPE_FIELD_NAME, get_name_for_t<T>()});
        },
        instance);
  }

  static void from_json(const nlohmann::json &j,
                        std::variant<typename Ts::member_type...> &instance) {
    std::string name;
    j.at(TYPE_FIELD_NAME).get_to(name);
    try_decode_name<Ts...>(name, instance, j);
  }

private:
  template <class Needle, class Rs1, class... Rs>
  static constexpr const char *get_name_for_t_impl()
    requires std::same_as<Needle, typename Rs1::member_type>
  {
    return Rs1::c_str();
  }

  template <class Needle, class Rs1, class... Rs>
  static constexpr const char *get_name_for_t_impl() {
    return get_name_for_t_impl<Needle, Rs...>();
  }

  template <class Rs1, class... Rs>
  static void
  try_decode_name(const std::string &name,
                  std::variant<typename Ts::member_type...> &instance,
                  const nlohmann::json &j) {
    if (Rs1::c_str() == name) {
      instance = j.get<typename Rs1::member_type>();
    } else {
      try_decode_name<Rs...>(name, instance, j);
    }
  }

  template <class... Rs1>
    requires(sizeof...(Rs1) == 0)
  static void try_decode_name(const std::string &name,
                              std::variant<typename Ts::member_type...> &,
                              const nlohmann::json &) {
    throw unknown_json_type_exception(name);
  }
};

} // namespace variant_json_helper

#endif /* VARIANT_JSON_HELPER_H */
